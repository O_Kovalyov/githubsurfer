package trailer.com.githubsurfer.presenter;

import android.util.Log;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import trailer.com.githubsurfer.App;
import trailer.com.githubsurfer.R;
import trailer.com.githubsurfer.service.Api;
import trailer.com.githubsurfer.service.RetrofitProvider;
import trailer.com.githubsurfer.model.Repo;

public class RepoActivityPresenter implements RepoPresenterInterface {
    private RepoActivityInterface loader;
    private RepoLoader repoLoader;

    public RepoActivityPresenter(RepoActivityInterface loader, RepoLoader repoLoader) {
        this.loader = loader;
        this.repoLoader = repoLoader;
    }

    @Override
    public void generateReposCall(final String repoOwner) {
        loader.progressBarShow();
        RetrofitProvider retrofitProvider = RetrofitProvider.getInstance();
        Api api = retrofitProvider.getApi();
        api.getRepos(repoOwner).enqueue(new Callback<List<Repo>>() {

            @Override
            public void onResponse(Call<List<Repo>> call, Response<List<Repo>> response) {
                loader.progressBarHide();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        repoLoader.repoLoadedSuccessfully(response.body());
                        loader.setReposCountToTitle(response.body().size());
                    } else {
                        Toast.makeText(App.getContext(), R.string.no_repos, Toast.LENGTH_LONG).show();
                    }
                } else {
                    repoLoader.onRepoLoadFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Repo>> call, Throwable t) {
                loader.progressBarHide();
                Log.e("Response", t.getMessage());
                repoLoader.onRepoLoadFailed(t.getMessage());
            }
        });
    }

    public String getActionBarText(String toolbarName) {
        StringBuilder actionBarText = new StringBuilder();
        actionBarText.append(toolbarName.toUpperCase().charAt(0));
        actionBarText.append(toolbarName.substring(1) + " " + App.getContext().getResources().getString(R.string.repositories));
        return actionBarText.toString();
    }

    public interface RepoLoader {
        void repoLoadedSuccessfully(List<Repo> repoList);

        void onRepoLoadFailed(String message);
    }
}
