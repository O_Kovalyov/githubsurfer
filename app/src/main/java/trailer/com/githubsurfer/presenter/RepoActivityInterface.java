package trailer.com.githubsurfer.presenter;


public interface RepoActivityInterface extends ProgressBarController{
    void setReposCountToTitle(int count);
}
