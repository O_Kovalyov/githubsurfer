package trailer.com.githubsurfer.presenter;

public interface ProgressBarController {
    void progressBarShow();
    void progressBarHide();
}
