package trailer.com.githubsurfer.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import trailer.com.githubsurfer.database.Database;
import trailer.com.githubsurfer.database.OrganizationRealmModel;
import trailer.com.githubsurfer.datamapper.OrganizationMapper;
import trailer.com.githubsurfer.service.Api;
import trailer.com.githubsurfer.service.RetrofitProvider;
import trailer.com.githubsurfer.model.Organization;

public class MainActivityPresenter implements MainActivityPresenterInterface {
    private OrganizationLoader loader;
    private MainActivityInterface pbLoader;

    public MainActivityPresenter(OrganizationLoader loader, MainActivityInterface pbLoader) {
        this.pbLoader = pbLoader;
        this.loader = loader;
    }

    @Override
    public void generateOrganizationsCall(String query) {
        pbLoader.progressBarShow();
        RetrofitProvider retrofitProvider = RetrofitProvider.getInstance();
        Api api = retrofitProvider.getApi();
        api.getOrganizationInfo(query).enqueue(new Callback<Organization>() {
            @Override
            public void onResponse(Call<Organization> call, Response<Organization> response) {
                if (response.isSuccessful()) {
                    loader.organizationLoaded(response.body());
                    if (Database.getInstance().getAllData(OrganizationRealmModel.class).size() < 10) {
                        Database.getInstance().saveSingleObject(new OrganizationMapper().map(response.body()));
                        pbLoader.progressBarHide();
                    } else {
                        Database.getInstance().deleteFirstElement(OrganizationRealmModel.class);
                        Database.getInstance().saveSingleObject(new OrganizationMapper().map(response.body()));
                        pbLoader.progressBarHide();
                    }
                } else {
                    loader.organizationLoadFailed(response.message());
                    Log.d("Response Error", String.valueOf(response.message()));
                    pbLoader.progressBarHide();
                }
            }

            @Override
            public void onFailure(Call<Organization> call, Throwable t) {
                Log.d("Response", t.getMessage());
                loader.organizationLoadFailed(t.getMessage());
                pbLoader.progressBarHide();
            }
        });
    }

    @Override
    public List<Organization> getRecentFound() {
        List<Organization> orgs = new ArrayList<>();
        if (Database.getInstance().hasData(OrganizationRealmModel.class)) {
            orgs.addAll(new OrganizationMapper()
                    .reverseMap(Database.getInstance().getAllData(OrganizationRealmModel.class)));
        }
        return orgs;
    }

    @Override
    public void clearDB() {
        Database.getInstance().clearDataBase();
    }

    public interface OrganizationLoader {
        void organizationLoaded(Organization organization);

        void organizationLoadFailed(String message);
    }
}
