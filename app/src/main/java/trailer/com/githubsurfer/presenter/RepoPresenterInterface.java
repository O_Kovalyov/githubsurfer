package trailer.com.githubsurfer.presenter;

import java.util.List;
import trailer.com.githubsurfer.model.Repo;

public interface RepoPresenterInterface {

    void generateReposCall(String repoOwner);
    String getActionBarText(String toolbarName);
}
