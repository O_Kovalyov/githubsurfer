package trailer.com.githubsurfer.presenter;

import android.view.View;

import java.util.List;

import trailer.com.githubsurfer.model.Organization;

public interface MainActivityPresenterInterface {
    void generateOrganizationsCall(String query);
    List<Organization> getRecentFound();
    void clearDB();
}
