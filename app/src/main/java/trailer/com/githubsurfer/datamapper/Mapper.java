package trailer.com.githubsurfer.datamapper;

import java.util.List;

public interface Mapper<T, E> {
    E map(T obj);
    T reverseMap(E obj);
    List<E> map(List<T> obj);
    List<T> reverseMap(List<E> obj);
}
