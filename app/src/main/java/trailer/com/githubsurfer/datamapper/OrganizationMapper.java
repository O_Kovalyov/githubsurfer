package trailer.com.githubsurfer.datamapper;

import java.util.ArrayList;
import java.util.List;

import trailer.com.githubsurfer.database.OrganizationRealmModel;
import trailer.com.githubsurfer.model.Organization;

public class OrganizationMapper implements Mapper<Organization, OrganizationRealmModel> {

    @Override
    public OrganizationRealmModel map(Organization obj) {
        OrganizationRealmModel model = new OrganizationRealmModel();
        model.setOrganizationBlogDbModel(obj.getOrganizationBlog());
        model.setOrganizationLocationDbModel(obj.getOrganizationLocation());
        model.setOrganizationLogoDbModel(obj.getOrganizationLogo());
        model.setOrganizationNameDbModel(obj.getOrganizationName());
        model.setOrganizationLoginDbModel(obj.getOrganizationLogin());
        return model;
    }

    @Override
    public Organization reverseMap(OrganizationRealmModel obj) {
        Organization model = new Organization();
        model.setOrganizationBlog(obj.getOrganizationBlogDbModel());
        model.setOrganizationLocation(obj.getOrganizationLocationDbModel());
        model.setOrganizationLogo(obj.getOrganizationLogoDbModel());
        model.setOrganizationName(obj.getOrganizationNameDbModel());
        model.setOrganizationLogin(obj.getOrganizationLoginDbModel());
        return model;
    }

    @Override
    public List<OrganizationRealmModel> map(List<Organization> obj) {
        List<OrganizationRealmModel> orgs = new ArrayList<>();
        for (Organization org : obj) {
            OrganizationRealmModel model = new OrganizationRealmModel();
            model.setOrganizationBlogDbModel(org.getOrganizationBlog());
            model.setOrganizationLocationDbModel(org.getOrganizationLocation());
            model.setOrganizationLogoDbModel(org.getOrganizationLogo());
            model.setOrganizationNameDbModel(org.getOrganizationName());
            model.setOrganizationLoginDbModel(org.getOrganizationLogin());
            orgs.add(model);
        }
        return orgs;
    }

    @Override
    public List<Organization> reverseMap(List<OrganizationRealmModel> obj) {
        List<Organization> orgs = new ArrayList<>();
        for (OrganizationRealmModel org : obj) {
            Organization model = new Organization();
            model.setOrganizationBlog(org.getOrganizationBlogDbModel());
            model.setOrganizationLocation(org.getOrganizationLocationDbModel());
            model.setOrganizationLogo(org.getOrganizationLogoDbModel());
            model.setOrganizationName(org.getOrganizationNameDbModel());
            model.setOrganizationLogin(org.getOrganizationLoginDbModel());
            orgs.add(model);
        }
        return orgs;
    }
}
