package trailer.com.githubsurfer.view;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import trailer.com.githubsurfer.R;
import trailer.com.githubsurfer.model.Repo;
import trailer.com.githubsurfer.presenter.RepoActivityInterface;
import trailer.com.githubsurfer.presenter.RepoActivityPresenter;
import trailer.com.githubsurfer.presenter.RepoPresenterInterface;
import trailer.com.githubsurfer.view.adapters.RepoRecyclerViewAdapter;

public class RepoActivity extends AppCompatActivity implements RepoActivityPresenter.RepoLoader, RepoActivityInterface {

    @BindView(R.id.repoRecyclerView)
    RecyclerView repoRecyclerView;
    @BindView(R.id.repoCircleProgressBar)
    ProgressBar progressBar;
    @BindView(R.id.noReposFoundTV)
    TextView notFoundTextView;
    private Intent intent;
    private RepoRecyclerViewAdapter repoAdapter;
    private ActionBar toolbar;
    private RepoPresenterInterface presenterInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_activity);
        ButterKnife.bind(this);
        presenterInterface = new RepoActivityPresenter(this, this);
        intent = getIntent();
        repoRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        repoAdapter = new RepoRecyclerViewAdapter();
        repoRecyclerView.setAdapter(repoAdapter);
        presenterInterface.generateReposCall(intent.getStringExtra("Organization"));
        toolbar = RepoActivity.this.getSupportActionBar();
        toolbar.setTitle(Html.fromHtml("<font color=\"#DCEDC8\">" + presenterInterface.getActionBarText(intent.getStringExtra("Organization")) + "</font>"));
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void repoLoadedSuccessfully(List<Repo> repoList) {
        repoAdapter.setData(repoList);
    }

    @Override
    public void onRepoLoadFailed(String message) {
        if (message.contains(this.getResources().getString(R.string.not_found))) {
            notFoundTextView.setVisibility(View.VISIBLE);
        } else if (message.contains(this.getResources().getString(R.string.forbidden))) {
            notFoundTextView.setVisibility(View.GONE);
            Toast.makeText(RepoActivity.this, R.string.out_of_limit, Toast.LENGTH_LONG).show();
        } else {
            notFoundTextView.setVisibility(View.GONE);
            Toast.makeText(RepoActivity.this, R.string.repo_loading_failed, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void setReposCountToTitle(int count) {
        toolbar.setTitle(Html.fromHtml("<font color=\"#DCEDC8\">" +
                presenterInterface.getActionBarText(intent.getStringExtra("Organization")) +
                " (" + count + ")" + "</font>"));
    }

    @Override
    public void progressBarShow() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void progressBarHide() {
        progressBar.setVisibility(View.GONE);
    }
}
