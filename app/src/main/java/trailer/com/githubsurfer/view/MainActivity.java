package trailer.com.githubsurfer.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import trailer.com.githubsurfer.App;
import trailer.com.githubsurfer.R;
import trailer.com.githubsurfer.model.Organization;
import trailer.com.githubsurfer.presenter.MainActivityInterface;
import trailer.com.githubsurfer.presenter.MainActivityPresenter;
import trailer.com.githubsurfer.presenter.MainActivityPresenterInterface;
import trailer.com.githubsurfer.view.adapters.OrganizationsRecyclerViewAdapter;
import trailer.com.githubsurfer.view.listener.RecyclerViewOnItemClickListener;

public class MainActivity extends AppCompatActivity implements MainActivityPresenter.OrganizationLoader, MainActivityInterface, View.OnClickListener {
    @BindView(R.id.organizationsRecyclerView)
    RecyclerView orgsRecyclerView;
    @BindView(R.id.enterOrganizationEditText)
    EditText organizationEnteredName;
    @BindView(R.id.circleProgressBar)
    ProgressBar progressBar;
    @BindView(R.id.titleTextView)
    TextView title;
    @BindView(R.id.loadFromDBButton)
    FloatingActionButton uploadButton;
    @BindView(R.id.clearDBButton)
    FloatingActionButton clearDBButton;
    @BindView(R.id.clearScreen)
    FloatingActionButton clearScreen;
    @BindView(R.id.notFound)
    TextView notFoundTextView;
    private MainActivityPresenterInterface presenter;
    private TextWatcher textWatcher;
    private OrganizationsRecyclerViewAdapter adapter;
    private boolean lastOperationLoadButton = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        adapter = new OrganizationsRecyclerViewAdapter();
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                adapter.clearRecyclerViewAdapter();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isOnline()) {
                    if (s.length() >= 3)
                        presenter.generateOrganizationsCall(s.toString());
                        lastOperationLoadButton = false;
                } else {
                    Toast.makeText(App.getContext(), R.string.no_internet, Toast.LENGTH_SHORT).show();
                }
            }
        };
        presenter = new MainActivityPresenter(this, this);
        orgsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        orgsRecyclerView.setAdapter(adapter);
        orgsRecyclerView.addOnItemTouchListener(new RecyclerViewOnItemClickListener(this,
                orgsRecyclerView, new RecyclerViewOnItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent intent = new Intent(MainActivity.this, RepoActivity.class);
                intent.putExtra("Organization", adapter.getOrganizationLogin(position));
                startActivity(intent);
            }
        }));
        organizationEnteredName.addTextChangedListener(textWatcher);
        clearDBButton.setOnClickListener(this);
        uploadButton.setOnClickListener(this);
        clearScreen.setOnClickListener(this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if(lastOperationLoadButton){
            adapter.setData(presenter.getRecentFound());
        }
    }

    @Override
    protected void onDestroy() {
        organizationEnteredName.removeTextChangedListener(textWatcher);
        super.onDestroy();
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public void organizationLoaded(Organization organization) {
        notFoundTextView.setVisibility(View.GONE);
        adapter.setData(organization);
    }

    @Override
    public void organizationLoadFailed(String message) {
        if (message.contains(this.getResources().getString(R.string.not_found))) {
            notFoundTextView.setVisibility(View.VISIBLE);
        } else if (message.contains(this.getResources().getString(R.string.forbidden))) {
            notFoundTextView.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, R.string.out_of_limit, Toast.LENGTH_LONG).show();
        } else {
            notFoundTextView.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, R.string.repo_loading_failed, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void progressBarShow() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void progressBarHide() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("load_button_clicked", lastOperationLoadButton);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        lastOperationLoadButton = savedInstanceState.getBoolean("load_button_clicked");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loadFromDBButton:
                if (presenter.getRecentFound().size() == 0)
                    Toast.makeText(this, R.string.db_empty, Toast.LENGTH_LONG).show();
                else
                    notFoundTextView.setVisibility(View.GONE);
                    organizationEnteredName.setText("");
                    adapter.setData(presenter.getRecentFound());
                    lastOperationLoadButton = true;
                break;
            case R.id.clearDBButton:
                presenter.clearDB();
                lastOperationLoadButton = false;
                break;
            case R.id.clearScreen:
                adapter.clearRecyclerViewAdapter();
                lastOperationLoadButton = false;
                break;
        }
    }
}
