package trailer.com.githubsurfer.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import trailer.com.githubsurfer.R;
import trailer.com.githubsurfer.model.Organization;

public class OrganizationsRecyclerViewAdapter extends RecyclerView.Adapter<OrganizationsRecyclerViewAdapter.ViewHolder> {
    private List<Organization> organizationList = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.organizationLogo)
        ImageView organizationLogo;
        @BindView(R.id.organizationName)
        TextView organizationName;
        @BindView(R.id.organizationBlog)
        TextView organizationBlog;
        @BindView(R.id.organizationLocation)
        TextView organizationLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public OrganizationsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.organizations_recyclerview_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizationsRecyclerViewAdapter.ViewHolder holder, int position) {

        if (organizationList.size() > 0 && organizationList.get(position).getOrganizationBlog() != null) {
            holder.organizationBlog.setText(organizationList.get(position).getOrganizationBlog());
        } else if(organizationList.get(position).getOrganizationBlog() == null) {
            holder.organizationBlog.setText(holder.organizationBlog.getContext().getResources().getString(R.string.missing_info));
        } else holder.organizationBlog.setText(holder.organizationBlog.getContext().getResources().getString(R.string.missing_info));
        if (organizationList.size() > 0 && organizationList.get(position).getOrganizationLocation() != null) {
            holder.organizationLocation.setText(organizationList.get(position).getOrganizationLocation());
        }  else if(organizationList.get(position).getOrganizationLocation() == null) {
            holder.organizationLocation.setText(holder.organizationBlog.getContext().getResources().getString(R.string.missing_info));
        } else holder.organizationLocation.setText(holder.organizationBlog.getContext().getResources().getString(R.string.missing_info));
        if (organizationList.size() > 0 && organizationList.get(position).getOrganizationName() != null) {
            holder.organizationName.setText(organizationList.get(position).getOrganizationName());
        } else if(organizationList.get(position).getOrganizationName() == null) {
            holder.organizationName.setText(holder.organizationBlog.getContext().getResources().getString(R.string.missing_info));
        } else holder.organizationName.setText(holder.organizationBlog.getContext().getResources().getString(R.string.missing_info));
            Glide.with(holder.organizationLogo.getContext()).load(organizationList.get(position).getOrganizationLogo()).into(holder.organizationLogo);
    }

    @Override
    public int getItemCount() {
        return organizationList.size();
    }

    public void setData(Organization organization) {
        organizationList.clear();
        organizationList.add(organization);
        notifyDataSetChanged();
    }

    public void setData(List<Organization> orgs){
        organizationList.clear();
        organizationList.addAll(orgs);
        notifyDataSetChanged();
    }

    public String getOrganizationLogin(int position){
        return organizationList.get(position).getOrganizationLogin();
    }

    public void clearRecyclerViewAdapter(){
        organizationList.clear();
        notifyDataSetChanged();
    }
}
