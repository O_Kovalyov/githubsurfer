package trailer.com.githubsurfer.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import trailer.com.githubsurfer.R;
import trailer.com.githubsurfer.model.Repo;

public class RepoRecyclerViewAdapter extends RecyclerView.Adapter<RepoRecyclerViewAdapter.RepoViewHolder> {
    private List<Repo> repoList = new ArrayList<>();

    public class RepoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.repoNameTextView)
        TextView repoNameTextView;
        @BindView(R.id.repoDescriptionTextView)
        TextView repoDescriptionTextView;

        RepoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_recyclerview_item, parent, false);
        return new RepoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
        holder.repoNameTextView.setText(repoList.get(position).getRepoName());
        if (repoList.get(position).getRepoDescription() != null) {
            if (repoList.get(position).getRepoDescription().length() < 500) {
                holder.repoDescriptionTextView.setText(repoList.get(position).getRepoDescription());
            } else {
                holder.repoDescriptionTextView.setText(holder.repoDescriptionTextView.getContext().getResources().getString(R.string.incorrect_description));
            }
        } else {
            holder.repoDescriptionTextView.setText(R.string.no_description);
        }
    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    public void setData(List<Repo> repositories) {
        repoList.clear();
        repoList.addAll(repositories);
        notifyDataSetChanged();
    }
}
