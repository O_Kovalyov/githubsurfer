package trailer.com.githubsurfer.database;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Database {
    private Realm realm;
    private static Database INSTANCE;

    public Database() {
        realm = Realm.getDefaultInstance();
    }

    public static Database getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Database();
        }
        return INSTANCE;
    }

    public void saveSingleObject(OrganizationRealmModel obj) {
        List<OrganizationRealmModel> list = new ArrayList<>();
        list.addAll(Database.getInstance().getAllData(OrganizationRealmModel.class, "organizationLoginDbModel", obj.getOrganizationLoginDbModel()));
        if (!list.contains(obj)) {
            realm.beginTransaction();
            realm.insert(obj);
            realm.commitTransaction();
        }
    }

    public <T extends RealmObject> List<T> getAllData(Class<T> classType) {
        List<T> dataList = new ArrayList<>();
        RealmResults<T> realmQuery = realm.where(classType)
                .findAll();
        dataList.addAll(realmQuery);
        return dataList;
    }

    public <T extends RealmObject> List<T> getAllData(Class<T> classType, String field, String key) {
        List<T> dataList = new ArrayList<>();
        RealmResults<T> realmQuery = realm.where(classType)
                .equalTo(field, key)
                .findAll();
        dataList.addAll(realmQuery);
        return dataList;
    }

    public <T extends RealmObject> void deleteFirstElement(Class<T> classType) {
        final RealmResults<T> results = realm.where(classType).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteFirstFromRealm();
            }
        });
    }

    public void clearDataBase() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    public <T extends RealmObject> boolean hasData(Class<T> classType) {
        return !realm.where(classType).findAll().isEmpty();
    }
}
