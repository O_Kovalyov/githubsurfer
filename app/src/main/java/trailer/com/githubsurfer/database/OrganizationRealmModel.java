package trailer.com.githubsurfer.database;

import java.util.Objects;

import io.realm.RealmObject;

public class OrganizationRealmModel extends RealmObject {
    private String organizationLogoDbModel;
    private String organizationNameDbModel;
    private String organizationBlogDbModel;
    private String organizationLocationDbModel;
    private String organizationLoginDbModel;

    public String getOrganizationLogoDbModel() {
        return organizationLogoDbModel;
    }

    public void setOrganizationLogoDbModel(String organizationLogoDbModel) {
        this.organizationLogoDbModel = organizationLogoDbModel;
    }

    public String getOrganizationNameDbModel() {
        return organizationNameDbModel;
    }

    public void setOrganizationNameDbModel(String organizationNameDbModel) {
        this.organizationNameDbModel = organizationNameDbModel;
    }

    public String getOrganizationBlogDbModel() {
        return organizationBlogDbModel;
    }

    public void setOrganizationBlogDbModel(String organizationBlogDbModel) {
        this.organizationBlogDbModel = organizationBlogDbModel;
    }

    public String getOrganizationLocationDbModel() {
        return organizationLocationDbModel;
    }

    public void setOrganizationLocationDbModel(String organizationLocationDbModel) {
        this.organizationLocationDbModel = organizationLocationDbModel;
    }

    public String getOrganizationLoginDbModel() {
        return organizationLoginDbModel;
    }

    public void setOrganizationLoginDbModel(String organizationLoginDbModel) {
        this.organizationLoginDbModel = organizationLoginDbModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrganizationRealmModel)) return false;
        OrganizationRealmModel that = (OrganizationRealmModel) o;
        return Objects.equals(organizationLogoDbModel, that.organizationLogoDbModel) &&
                Objects.equals(organizationNameDbModel, that.organizationNameDbModel) &&
                Objects.equals(organizationBlogDbModel, that.organizationBlogDbModel) &&
                Objects.equals(organizationLocationDbModel, that.organizationLocationDbModel) &&
                Objects.equals(organizationLoginDbModel, that.organizationLoginDbModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(organizationLogoDbModel, organizationNameDbModel, organizationBlogDbModel, organizationLocationDbModel, organizationLoginDbModel);
    }

    @Override
    public String toString() {
        return "OrganizationRealmModel{" +
                "organizationLogoDbModel='" + organizationLogoDbModel + '\'' +
                ", organizationNameDbModel='" + organizationNameDbModel + '\'' +
                ", organizationBlogDbModel='" + organizationBlogDbModel + '\'' +
                ", organizationLocationDbModel='" + organizationLocationDbModel + '\'' +
                ", organizationLoginDbModel='" + organizationLoginDbModel + '\'' +
                '}';
    }
}
