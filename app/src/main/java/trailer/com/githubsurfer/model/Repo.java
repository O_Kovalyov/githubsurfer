package trailer.com.githubsurfer.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Repo {

    @SerializedName("name")
    private String repoName;
    @SerializedName("description")
    private String repoDescription;
    @SerializedName("id")
    private String repoId;
    @SerializedName("full_name")
    private String repoFullName;

    public Repo(String repoName, String repoDescription, String repoId, String repoFullName) {
        this.repoName = repoName;
        this.repoDescription = repoDescription;
        this.repoId = repoId;
        this.repoFullName = repoFullName;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoDescription() {
        return repoDescription;
    }

    public void setRepoDescription(String repoDescription) {
        this.repoDescription = repoDescription;
    }

    public String getRepoId() {
        return repoId;
    }

    public void setRepoId(String repoId) {
        this.repoId = repoId;
    }

    public String getRepoFullName() {
        return repoFullName;
    }

    public void setRepoFullName(String repoFullName) {
        this.repoFullName = repoFullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Repo)) return false;
        Repo repo = (Repo) o;
        return Objects.equals(repoName, repo.repoName) &&
                Objects.equals(repoDescription, repo.repoDescription) &&
                Objects.equals(repoId, repo.repoId) &&
                Objects.equals(repoFullName, repo.repoFullName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(repoName, repoDescription, repoId, repoFullName);
    }

    @Override
    public String toString() {
        return "Repo{" +
                "repoName='" + repoName + '\'' +
                ", repoDescription='" + repoDescription + '\'' +
                ", repoId='" + repoId + '\'' +
                ", repoFullName='" + repoFullName + '\'' +
                '}';
    }
}
