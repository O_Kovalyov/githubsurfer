package trailer.com.githubsurfer.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Organization {


    @SerializedName("avatar_url")
    private String organizationLogo;
    @SerializedName("name")
    private String organizationName;
    @SerializedName("blog")
    private String organizationBlog;
    @Nullable
    @SerializedName("location")
    private String organizationLocation;
    @SerializedName("login")
    private String organizationLogin;

    public Organization() {
    }

    public Organization( String organizationLogo, String organizationName,
                        String organizationBlog, @Nullable String organizationLocation, String organizationLogin) {

        this.organizationLogo = organizationLogo;
        this.organizationName = organizationName;
        this.organizationBlog = organizationBlog;
        this.organizationLocation = organizationLocation;
        this.organizationLogin = organizationLogin;
    }


    public String getOrganizationLogo() {
        return organizationLogo;
    }

    public void setOrganizationLogo(String organizationLogo) {
        this.organizationLogo = organizationLogo;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationBlog() {
        return organizationBlog;
    }

    public void setOrganizationBlog(String organizationBlog) {
        this.organizationBlog = organizationBlog;
    }

    @Nullable
    public String getOrganizationLocation() {
        return organizationLocation;
    }

    public void setOrganizationLocation(@Nullable String organizationLocation) {
        this.organizationLocation = organizationLocation;
    }

    public String getOrganizationLogin() {
        return organizationLogin;
    }

    public void setOrganizationLogin(String organizationLogin) {
        this.organizationLogin = organizationLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Organization)) return false;
        Organization that = (Organization) o;
        return Objects.equals(organizationLogo, that.organizationLogo) &&
                Objects.equals(organizationName, that.organizationName) &&
                Objects.equals(organizationBlog, that.organizationBlog) &&
                Objects.equals(organizationLocation, that.organizationLocation) &&
                Objects.equals(organizationLogin, that.organizationLogin);
    }

    @Override
    public int hashCode() {

        return Objects.hash(organizationLogo, organizationName, organizationBlog, organizationLocation, organizationLogin);
    }

    @Override
    public String toString() {
        return "Organization{" +
                " organizationLogo='" + organizationLogo + '\'' +
                ", organizationName='" + organizationName + '\'' +
                ", organizationBlog='" + organizationBlog + '\'' +
                ", organizationLocation='" + organizationLocation + '\'' +
                ", organizationLogin='" + organizationLogin + '\'' +
                '}';
    }
}
