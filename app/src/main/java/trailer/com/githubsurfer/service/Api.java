package trailer.com.githubsurfer.service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import trailer.com.githubsurfer.model.Organization;
import trailer.com.githubsurfer.model.Repo;

public interface Api {

    @GET ("/orgs/{org}")
    Call<Organization> getOrganizationInfo(@Path("org") String org);

    @GET("/orgs/{org}/repos")
    Call<List<Repo>> getRepos(@Path("org") String org);
}
