package trailer.com.githubsurfer.service;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider {

    private static final String ENDPOINT = "https://api.github.com/";
    private static RetrofitProvider INSTANCE;
    private Retrofit retrofit;
    private Api api;

    private RetrofitProvider() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
        api = retrofit.create(Api.class);
    }

    public static RetrofitProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RetrofitProvider();
            return INSTANCE;
        } else {
            return INSTANCE;
        }
    }

    public Api getApi() {
        return api;
    }

    private OkHttpClient getClient() {
        return new OkHttpClient();
    }
}
